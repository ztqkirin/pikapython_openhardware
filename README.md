# PikaPython-OpenHardware

PikaPython 开源硬件仓库

## 开源协议：GPL 3.0

使用了 PikaPython-OpenHardware 的衍生项目也 **必须开源**，且 **也使用 GPL3.0 协议**。

# PIKA 派 - WIRELESS (验证中)

![9bd0a46068c3f96db0ad85e40709d6d](assets/9bd0a46068c3f96db0ad85e40709d6d.png)

为了嵌入式Python速成班的学习使用设计的开发板，带有WIFI、4G、LCD、RS485、板载温湿度传感器、蜂鸣器等。

为了方便大家复刻，开发板的所有原件均是立创可贴的（除了485的防雷原件，可不贴）。

开发板的 PikaPython 固件和例程将会逐步发布。

参与该开发板设计的主要贡献者：

- 冰点（微信名）—— 元件选型、原理图、PCB设计
- 方海钰 —— 产品设计、元件选型
- [Kirin](https://gitee.com/ztqkirin) —— 硬件验证、驱动设计
- [HonestQiao](https://github.com/HonestQiao) —— 硬件验证

## 开发板底版(PIKA_PI_WIRELESS)：

- 主控：ESP32S3
- 4G：FS800
- rs485
- 蜂鸣器
- nand FLASH
- 继电器
- 12V供电
- 温度传感器
- 光敏电阻

## LCD拓展板(PIKA_PI_WIRELESS_LCD)：

- 240*320 LCD屏
- 按键*4

![3769ab4d3c57bd9d48ca4e07b48603a](assets/3769ab4d3c57bd9d48ca4e07b48603a.png)
